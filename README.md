# Ansible Role: Kubeseal

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kubeseal/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kubeseal/-/commits/main)

This role installs [Kubeseal](https://github.com/bitnami-labs/sealed-secrets) binary on any supported host.

## NOTE

This role works with ansible==4.10.0. If you're using ansible > 4.10.0 you probably get a bug and the unarchive tasks won't work. To install ansible 4.10.0 you will try this:

```
sudo pip3 install ansible==4.10.0
```

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kubeseal_version: latest # tag v0.17.2 if you want a specific version
    setup_dir: /tmp
    kubeseal_arch: amd64 # amd64, arm, arm64
    kubeseal_bin_path: /usr/local/bin
    kubeseal_repo_path: https://github.com/bitnami-labs/sealed-secrets/releases/download

This role can install the latest or a specific version. See [available kubeseal releases](https://github.com/bitnami-labs/sealed-secrets) and change this variable accordingly.

    kubeseal_version: latest # tag v0.17.2 if you want a specific version

The path of the kubeseal repository.

    kubeseal_repo_path: https://github.com/bitnami-labs/sealed-secrets/releases/download

The path to the home kubeseal directory.

    kubeseal_bin_path: /usr/local/bin

Kubeseal supports amd64, arm and arm64 CPU architectures, just change for the main architecture of your CPU.

    kubeseal_arch: amd64 # amd64, arm, arm64

Kubeseal needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Kubeseal. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kubeseal

## License

MIT / BSD
